package app

import (
	exampleweb "gitlab.com/aerilyn/skeleton-golang/internal/app/example/delivery/web"
	healthcheckweb "gitlab.com/aerilyn/skeleton-golang/internal/app/healthcheck/delivery/web"
)

// jhipster-needle-import-handler

type RequiredHandlers struct {
	HealthCheckHTTPHandlerRegistry *healthcheckweb.HealthCheckHandlerRegistry
	ExampleHTTPHandlerRegistry     *exampleweb.ExampleHandlerRegistry
}
