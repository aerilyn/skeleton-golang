package healthcheck

import (
	"gitlab.com/aerilyn/skeleton-golang/internal/app/healthcheck/delivery/web"

	"github.com/google/wire"
)

var ModuleSet = wire.NewSet(
	web.NewHandlerRegistry,
)
