package example

import (
	"gitlab.com/aerilyn/skeleton-golang/internal/app/example/delivery/web"
	"gitlab.com/aerilyn/skeleton-golang/internal/app/example/usecase"
	"gitlab.com/aerilyn/skeleton-golang/internal/app/example/usecase/usecaseimpl"

	"github.com/google/wire"
)

var ModuleSet = wire.NewSet(
	repositoryModuleSet,
	validatorModuleSet,
	usecaseModuleSet,
	deliveryModuleSet,
)

var repositoryModuleSet = wire.NewSet()

var deliveryModuleSet = wire.NewSet(
	wire.Struct(new(web.ExampleRegistryOptions), "*"),
	web.NewExampleHandlerRegistry,
)

var validatorModuleSet = wire.NewSet()

var usecaseModuleSet = wire.NewSet(
	wire.Struct(new(usecaseimpl.CreateOptions), "*"),
	usecaseimpl.NewExample,
	wire.Bind(new(usecase.Example), new(*usecaseimpl.Example)),
)
