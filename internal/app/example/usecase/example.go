package usecase

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
)

type ExampleDTO struct {
	Status bool `json:"status"`
}

type Example interface {
	Example(ctx context.Context) (*ExampleDTO, errors.CodedError)
}
