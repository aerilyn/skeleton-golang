package usecaseimpl

import (
	"context"

	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/skeleton-golang/internal/app/example/usecase"
)

type CreateOptions struct {
	Config config.ConfigEnv
}

type Example struct {
	options CreateOptions
}

func NewExample(options CreateOptions) *Example {
	return &Example{
		options: options,
	}
}

func (s *Example) Example(ctx context.Context) (*usecase.ExampleDTO, errors.CodedError) {
	dto := usecase.ExampleDTO{
		Status: true,
	}
	return &dto, nil
}
