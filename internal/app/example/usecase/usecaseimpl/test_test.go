package usecaseimpl

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	configMock "gitlab.com/aerilyn/service-library/config/mock"
	"gitlab.com/aerilyn/skeleton-golang/internal/app/example/usecase"
)

func TestItemCreate(t *testing.T) {
	ctx := context.Background()
	dto := usecase.ExampleDTO{
		Status: true,
	}
	Convey("Testing Example Test", t, func() {
		mockCtrl := gomock.NewController(t)
		Reset(func() {
			mockCtrl.Finish()
		})
		configMock := configMock.NewMockConfigEnv(mockCtrl)
		createOptions := CreateOptions{
			Config: configMock,
		}
		uc := NewExample(createOptions)
		Convey("when all process not return error  should return error nil", func() {
			res, errCode := uc.Example(ctx)
			So(res, ShouldNotBeNil)
			So(errCode, ShouldBeNil)
			So(res, ShouldResemble, &dto)
		})
	})
}
