package web

import (
	"gitlab.com/aerilyn/skeleton-golang/internal/app/example/usecase"

	"github.com/go-chi/chi"
)

type ExampleRegistryOptions struct {
	ExampleTest usecase.Example
}

type ExampleHandlerRegistry struct {
	options ExampleRegistryOptions
}

func NewExampleHandlerRegistry(options ExampleRegistryOptions) *ExampleHandlerRegistry {
	return &ExampleHandlerRegistry{
		options: options,
	}
}

func (h *ExampleHandlerRegistry) RegisterRoutesTo(r chi.Router) error {
	r.Route("/example/test", func(r chi.Router) {
		r.Get("/", ExampleTestWebHandler(h.options.ExampleTest))
	})
	return nil
}
