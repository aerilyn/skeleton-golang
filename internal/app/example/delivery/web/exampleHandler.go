package web

import (
	"net/http"

	"gitlab.com/aerilyn/service-library/log"
	"gitlab.com/aerilyn/service-library/response"
	"gitlab.com/aerilyn/skeleton-golang/internal/app/example/usecase"
)

func ExampleTestWebHandler(uc usecase.Example) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		result, err := uc.Example(ctx)
		if err != nil {
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}
		response.Response(log.ResponseOpts{w, r, ctx, result, http.StatusCreated, nil})
	}
}
