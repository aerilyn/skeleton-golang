package gmail

const (
	FromKey    string = "From"
	ToKey      string = "To"
	CcKey      string = "Cc"
	SubjectKey string = "Subject"
	BodyHtml   string = "text/html"
)
