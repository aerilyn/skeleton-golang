FROM golang:1.18-alpine3.15 AS builder

RUN apk update && apk add --no-cache git

WORKDIR $GOPATH/src/application/

COPY . .

ENV GOSUMDB=off
COPY go.mod .
COPY go.sum .
RUN go mod download

RUN cd $GOPATH/src/application/cmd/app/ && GOOS=linux GOARCH=amd64 go build  -o /go/bin/main
FROM alpine:3.11

RUN apk add --no-cache tzdata

COPY --from=builder /go/bin/main /go/bin/main

EXPOSE 3000

ENTRYPOINT ["/go/bin/main"]